<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Film;
use App\Genre;
use File;

class FilmController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show');
    }

    public function create()
    {
        $genre = Genre::all();
        return view('/film/create', compact('genre'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'sinopsis' => 'required',
            'tahun' => 'required',
            'genre_id' => 'required',
            'poster' => 'required|mimes:png,jpg,jpeg'
        ]);
        
        $fileName = time().'.'.$request->poster->extension();

        $genre = new Film;

        $genre->judul = $request->judul;
        $genre->sinopsis = $request->sinopsis;
        $genre->tahun = $request->tahun;
        $genre->genre_id = $request->genre_id;
        $genre->poster = $fileName;

        $genre->save();

        $request->poster->move(public_path('/image'), $fileName);

        return redirect('/film');
    }

    public function index()
    {
        $film = Film::all();
        //dd($buku);
        return view('film.index', compact('film'));
    }

    public function show($id)
    {
        $film = Film::findorfail($id);
        return view('film.show', compact('film'));
    }

    public function edit($id)
    {
        $genre = Genre::all();
        $film = Film::findorfail($id);
        return view('film.edit', compact('film','genre'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'sinopsis' => 'required',
            'tahun' => 'required',
            'genre_id' => 'required',
            'poster' => 'mimes:png,jpg,jpeg',
        ]);

        $film = Film::findorfail($id);

        if ($request->has('poster')) {
            //$post>delete();
            $path = "image/";
            File::delete($path . $film->poster);
            $fileName = time().'.'.$request->poster->extension();   
            $request->poster->move(public_path('image'), $fileName);
            
            $film_data = [
                'judul' => $request->judul,
                'sinopsis' => $request->sinopsis,
                'tahun' => $request->tahun,
                'genre_id' => $request->genre_id,
                'poster' => $fileName,
            ];   
        } else {
            $film_data = [
                'judul' => $request->judul,
                'sinopsis' => $request->sinopsis,
                'tahun' => $request->tahun,
                'genre_id' => $request->genre_id,
            ];
        }

        $film->update($film_data);

        return redirect('/film'); 

    }

    public function destroy($id)
    {
        $film = Film::findorfail($id);

        $path = "image/";
        File::delete($path . $film->poster);
        $film->delete();

        return redirect('/film');

    }
}