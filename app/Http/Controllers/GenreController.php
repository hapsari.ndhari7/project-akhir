<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Genre;

class GenreController extends Controller
{
    public function create()
    {
        return view('genre.buat');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required'
        ]);
        
        $genre = Genre::create([
            "nama" => $request["nama"]
        ]);

        return redirect('/genre')->with('success', 'Buku Berhasil Disimpan');
    }

    public function index()
    {
        $genre = Genre::all();
        //dd($buku);
        return view('genre.ndex', compact('genre'));
    }

    public function show($id)
    {
        $genre = Genre::find($id);
        
        return view('genre.tampil', compact('genre'));
    }

    public function edit($id)
    {
        $genre = Genre::find($id);

        return view('genre.ubah', compact('genre'));
    }

    public function update(Request $request, $id)
    {
        $update = Genre::where('id', $id)->update([
            "nama" => $request["nama"]
        ]);
        return redirect('/genre')->with('success', 'Berhasil Update Buku');
    }

    public function destroy($id)
    {
        Genre::destroy($id);

        return redirect('/genre')->with('success', 'Buku Berhasil Dihapus');
    }
}
