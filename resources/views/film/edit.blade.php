@extends('template.master')

@section('title')
    <h3>Edit Film</h3>
@endsection

@section('content')
    <form action="/film/{{$film->id}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Judul</label>
            <input type="text" class="form-control" value="{{$film->judul}}" name="judul" placeholder="Enter Judul">
                @error('judul')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label>Sinopsis</label>
                <textarea class="form-control" name="sinopsis" placeholder="Enter Sinopsis" cols="30" rows="10">{{$film->sinopsis}}</textarea>
                @error('sinopsis')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label>Tahun</label>
                <input type="text" class="form-control" value="{{$film->tahun}}" name="tahun" placeholder="Enter Tahun">
                @error('tahun')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label>Genre</label>
                <select name="genre_id" class="form-control">
                    <option>-- Pilih Genre --</option>
                    @foreach ($genre as $item)
                        @if($item->id === $film->genre_id)
                            <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                        @else
                            <option value="{{$item->id}}">{{$item->nama}}</option>
                        @endif         
                    @endforeach
                </select>
                @error('genre_id')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label>Poster</label>
                <input type="file" class="form-control" name="poster">
                @error('poster')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>
      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
    </form>
</div>
@endsection