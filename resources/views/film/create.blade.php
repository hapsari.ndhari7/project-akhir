@extends('template.master')

@section('title')
    <h3>Tambah Film</h3>
@endsection

@section('content')
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Create List Film</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/film" method="POST" enctype="multipart/form-data">
        @csrf
      <div class="card-body">
        <div class="form-group">
          <label>Judul</label>
          <input type="text" class="form-control" name="judul" placeholder="Enter Judul">
            @error('judul')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label>Sinopsis</label>
            <textarea class="form-control" name="sinopsis" placeholder="Enter Sinopsis" cols="30" rows="10"></textarea>
            @error('sinopsis')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label>Tahun</label>
            <input type="text" class="form-control" name="tahun" placeholder="Enter Tahun">
              @error('tahun')
                  <div class="alert alert-danger">{{ $message }}</div>
              @enderror
          </div>
          <div class="form-group">
            <label>Genre</label>
            <select name="genre_id" class="form-control" id="">
              <option value="">-- Pilih Genre --</option>
              @foreach ($genre as $item)
                <option value="{{$item->id}}">{{$item->nama}}</option>
              @endforeach
            </select>
              @error('genre_id')
                  <div class="alert alert-danger">{{ $message }}</div>
              @enderror
          </div>
          <div class="form-group">
            <label>Poster</label>
            <input type="file" class="form-control" name="poster">
              @error('poster')
                  <div class="alert alert-danger">{{ $message }}</div>
              @enderror
          </div>
       </div>
      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Create</button>
      </div>
    </form>
</div>
@endsection
