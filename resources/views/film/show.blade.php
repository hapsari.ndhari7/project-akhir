@extends('template.master')

@section('title')
    Detail film
@endsection

@section('content')

<img src="{{asset('image/'.$film->poster)}}" alt="">
<h1>{{$film->judul}}</h1>
<p>{{$film->sinopsis}}</p>
    
@endsection