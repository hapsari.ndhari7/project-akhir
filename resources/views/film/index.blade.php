@extends('template.master')

@section('title')
    <h3>Halaman Film</h3>
@endsection

@section('content')

@auth
<a href="film/create" class="btn btn-primary my-2">Tambah</a>
@endauth
                <div class="row">
                    @forelse ($film as $item)
                        <div class="col-2">
                            <div class="card">
                                <img src="{{asset('image/'.$item->poster)}}" class="card-img-top" alt="...">
                                <div class="card-body">
                                    <h5>{{$item->judul}}</h5>
                                    <p class="card-text">{{Str::limit($item->sinopsis, 20)}}</p>
                                    @auth

                                    <form action="{{route('film.destroy', $item->id)}}" method="POST">
                                    @csrf
                                    @method('delete')
                                      <a href="{{route('film.show', $item->id)}}" class="btn btn-info btn-sm">Detail</a>
                                      <a href="{{route('film.edit', $item->id)}}" class="btn btn-warning btn-sm">Edit</a>
                                      <input type="submit" class="btn btn-danger btn-sm mt-1" value="Delete">
                                    </form>
                                        
                                    @endauth
                                    @guest
                                    <a href="{{route('film.show', $item->id)}}" class="btn btn-info btn-sm">Detail</a>
                                    @endguest
                                </div>
                            </div>
                        </div>          
                    @empty
                        <h1>Tidak Ada Film</h1>       
                    @endforelse
                </div>
         

@endsection