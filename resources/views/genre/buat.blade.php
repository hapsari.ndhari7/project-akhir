@extends('template.master')

@section('title')
    Tambah Genre
@endsection

@section('content')
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Create List Genre</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/genre" method="POST">
        @csrf
      <div class="card-body">
        <div class="form-group">
          <label>Nama</label>
          <input type="text" class="form-control" name="nama" placeholder="Enter genre">
            @error('nama')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
       </div>
      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Create</button>
      </div>
    </form>
</div>
@endsection